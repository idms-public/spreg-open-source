FROM openjdk:8

ADD . /srv/idms/applications/spreg
RUN apt-get update; apt-get install -y vim
RUN apt-get update; apt-get install -y openssh-server
WORKDIR /srv/idms/applications/spreg

#Try volume approach
# RUN mkdir -p /srv/idms/applications/spreg/build/install/spreg/shibboleth-test

RUN ./gradlew installDist
# RUN mkdir -p /srv/idms/applications/spreg/build/install/spreg/shibboleth-test
# Comment out to try with volumes
# RUN mkdir /srv/repos/
# RUN svnadmin create /srv/repos/svn
# RUN svn import /srv/idms/applications/spreg/build/install/spreg/shibboleth-test file:///srv/repos/svn/shibboleth-test -m "initial import"
# RUN ssh-keygen -t rsa -N '' -f /root/.ssh/id_rsa
