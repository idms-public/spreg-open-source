package edu.duke.oit.idms.shibboleth.spreg;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.ISVNEditor;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.io.diff.SVNDeltaGenerator;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

/**
 * @author shilen
 */
public class SVNManager {
  
  /**
   * @param filePath
   * @return xml
   * @throws SVNException
   * @throws IOException 
   */
  public static String getFile(String filePath) throws SVNException, IOException {

    String url = SPRegConfig.getInstance().getProperties().getProperty("svn.v3.url");
    String username = SPRegConfig.getInstance().getProperties().getProperty("svn.username");
    String password = SPRegConfig.getInstance().getProperties().getProperty("svn.password");

    SVNRepository repository = null;
      SVNRepositoryFactoryImpl.setup();
      repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(url));
      // ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(username, password);
    ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager();

    repository.setAuthenticationManager(authManager);

      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      repository.getFile(filePath, -1, null, baos);
      String xml = baos.toString();
      
      return xml;
  }
  
  /**
   * @param filePath
   * @param xmlOrig
   * @param xmlNew
   * @param commitMessage 
   * @throws Exception 
   */
  public static void commitFile(String filePath, String xmlOrig, String xmlNew, String commitMessage) throws Exception {

    //commitFile(SPRegConfig.getInstance().getProperties().getProperty("svn.url"), filePath, xmlOrig, xmlNew, commitMessage);
    commitFile(SPRegConfig.getInstance().getProperties().getProperty("svn.v3.url"), filePath, xmlOrig, xmlNew, commitMessage);
  }
  
  /**
   * @param url
   * @param filePath
   * @param xmlOrig
   * @param xmlNew
   * @param commitMessage 
   * @throws Exception 
   */
  public static void commitFile(String url, String filePath, String xmlOrig, String xmlNew, String commitMessage) throws Exception {

    String username = SPRegConfig.getInstance().getProperties().getProperty("svn.username");
    String password = SPRegConfig.getInstance().getProperties().getProperty("svn.password");

    try {
      SVNRepository repository = null;
      SVNRepositoryFactoryImpl.setup();
      repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(url));
      //ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(username, password);
      ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager();


      repository.setAuthenticationManager(authManager);
  
      ISVNEditor editor = repository.getCommitEditor(commitMessage, null);
      editor.openRoot(-1);
      String[] paths = filePath.split("/");
      String currPath = "";
      for (int i = 0; i < paths.length - 1; i++) {
        if (i != 0) {
          currPath += "/";
        }
        currPath += paths[i];
        editor.openDir(currPath, -1);
      }
        
      editor.openFile(filePath, -1);
      editor.applyTextDelta(filePath, null);
      SVNDeltaGenerator deltaGenerator = new SVNDeltaGenerator();
      String checksum = deltaGenerator.sendDelta(filePath, new ByteArrayInputStream(xmlOrig.getBytes()), 0, 
          new ByteArrayInputStream(xmlNew.getBytes()), editor, true);
  
      editor.closeFile(filePath, checksum);
        
      for (int i = 0; i < paths.length - 1; i++) {
        editor.closeDir();
      }
        
      editor.closeDir();
      editor.closeEdit();
    } catch (SVNException e) {
      throw new Exception("Error while trying to commit " + filePath, e);
    }
  }
}
