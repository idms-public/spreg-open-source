package edu.duke.oit.idms.shibboleth.spreg;

/**
 * @author shilen
 */
public class AttributeValue {
  private boolean isRegex;
  private String value;
  
  /**
   * @param value
   * @param isRegex
   */
  public AttributeValue(String value, boolean isRegex) {
    this.value = value;
    this.isRegex = isRegex;
  }
  
  /**
   * @return boolean
   */
  public boolean isRegex() {
    return isRegex;
  }
  
  /**
   * @return value
   */
  public String getValue() {
    return value;
  }
}
