package edu.duke.oit.idms.shibboleth.spreg;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


class Shutdown extends Thread {
	private SPReg spreg;
	public Shutdown(SPReg spreg) {
		super();
		this.spreg = spreg;
	}
	public void run() {
		spreg.LOG.error("SHUTDOWN: Received shutdown signal.");
		spreg.isShuttingDown = true;
		while (spreg.isShutDown == false) {
			try {
				Thread.sleep(2000);
			} catch (Exception e) {
				// this shouldn't happen...
			}
		}
		spreg.LOG.info("SHUTDOWN: Finished running shutdown routine.");
	}
}

/**
 * @author shilen
 */
public class SPReg {

	protected Logger LOG = Logger.getLogger(SPReg.class);
	protected boolean isShuttingDown = false;
	protected boolean isShutDown = false;
	private Connection conn = null;
	private PreparedStatement psSelectPendingSPs = null;
	private PreparedStatement psSelectPendingAssertionConsumerServices = null;
	private PreparedStatement psSelectPendingAttrsDuringMetadataUpdate = null;
	private PreparedStatement psSelectPendingAttrsNotDuringMetadataUpdate = null;
	private PreparedStatement psDeletePendingSP = null;
	private PreparedStatement psDeletePendingAssertionConsumerServices = null;
	private PreparedStatement psUpdateSP = null;
	private PreparedStatement psDeleteSP = null;
	private PreparedStatement psDeleteContacts = null;
	private PreparedStatement psDeleteUserSPs = null;
	private PreparedStatement psDeletePendingAttrs = null;
	private PreparedStatement psDeletePendingAttrsApproved = null;
	private PreparedStatement psDeleteApprovedAttrs = null;
	private ResultSet rsPendingSPs = null;
	private ResultSet rsPendingAssertionConsumerServices = null;
	private ResultSet rsPendingAttrsDuringMetadataUpdate = null;
	private ResultSet rsPendingAttrsNotDuringMetadataUpdate = null;

	private long lastErrorEmail = 0;

	/**
	 * @param args
	 */
	public static void main(String args[]) {
		PropertyConfigurator.configure(SPReg.class.getClassLoader().getResource("log4j.properties"));
		SPReg spreg = new SPReg();

		try {
			Runtime.getRuntime().addShutdownHook(new Shutdown(spreg));
			spreg.init();
			while (spreg.isShuttingDown == false) {
				spreg.LOG.info("Looking for pending changes...");
				spreg.processPendingAttrChanges();
				spreg.processPendingMetadataChanges();
				Thread.sleep(10000);
			}
		} catch (Exception e) {
			spreg.LOG.fatal("Shutting down.  Error while running.", e);
		} finally {
			spreg.shutdown();
		}
	}

	private void processPendingAttrChanges() {
		LOG.info("starting processPendingAttrChanges()");
		try {
			try {
				// this is the first DB query.  the connection may have timed out.
				rsPendingAttrsNotDuringMetadataUpdate = psSelectPendingAttrsNotDuringMetadataUpdate.executeQuery();
			} catch (SQLException e) {
				// try reconnecting
				shutdown();
				init();
				rsPendingAttrsNotDuringMetadataUpdate = psSelectPendingAttrsNotDuringMetadataUpdate.executeQuery();
			}

			String oldAttributeRelease = null;
			String oldAttributeResolver = null;
			String oldRelyingParty = null;
			String newAttributeRelease = null;
			String newRelyingParty = null;
			String lastEntityId = null;
			String oldMetadata = null;
			String newMetadata = null;
			int lastSpId = -1;

			while (rsPendingAttrsNotDuringMetadataUpdate.next()) {

				int spId = rsPendingAttrsNotDuringMetadataUpdate.getInt("sp_id");
				String entityId = rsPendingAttrsNotDuringMetadataUpdate.getString("entity_id");
				String attributeName = rsPendingAttrsNotDuringMetadataUpdate.getString("name");
				String value = rsPendingAttrsNotDuringMetadataUpdate.getString("value");
				boolean isRegex = rsPendingAttrsNotDuringMetadataUpdate.getBoolean("is_value_regex");

				if (entityId == null || entityId.trim().equals("")) {
					throw new RuntimeException("Got a null or empty entityId for spId=" + spId);
				}

				if (lastSpId != -1 && lastSpId != spId) {

//					Boolean deploy = false;

					// newRelyingParty is only updated if there's a nameid change
					if (newRelyingParty != null && !oldRelyingParty.equals(newRelyingParty)) {
						LOG.debug("committing new relyingparty policy #1");
						LOG.info("Updating relying party policy for " + lastEntityId);
						FileManager.saveFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.relyingParty"), newRelyingParty);
//						SVNManager.commitFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.relyingParty"), oldRelyingParty, newRelyingParty, "Updating relying party policy for " + lastEntityId);
						//deploy = true;
					}

					if (newMetadata != null && !oldMetadata.equals(newMetadata)) {
						LOG.debug("committing new metadata #1");
						LOG.info("Updating  metatadata for " + lastEntityId);
						FileManager.saveFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.metadata"), newRelyingParty);
//						SVNManager.commitFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.metadata"), oldRelyingParty, newRelyingParty, "Updating  metatadata for " + lastEntityId);
						//deploy = true;

					}

					if (!oldAttributeRelease.equals(newAttributeRelease)) {
						LOG.debug("commiting new attributerelease #1");
						LOG.info("Updating attribute release policy for " + lastEntityId);
						FileManager.saveFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.attributeFilter"), newAttributeRelease);
//						SVNManager.commitFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.attributeFilter"), oldAttributeRelease, newAttributeRelease, "Updating attribute release policy for " + lastEntityId);
						//deploy = true;
					}

//					if (deploy) {
//						DeployUsingSVN.deployAll();
//						LOG.info("deployed all in processPendingAttrChanges #1");
//					}

					psDeletePendingAttrs.setInt(1, lastSpId);
					psDeletePendingAttrs.executeUpdate();
					conn.commit();
					oldAttributeRelease = null;
					newAttributeRelease = null;
					newRelyingParty = null;
					LOG.info("Done working on " + lastEntityId + ".");
				}

				if (oldAttributeRelease == null) {
					//oldAttributeRelease = SVNManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.attributeFilter"));
					oldAttributeRelease = FileManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.attributeFilter"));
					SchemaValidation.validate(oldAttributeRelease);
					LOG.info("validated attributeFilter");
				}

				//oldAttributeResolver = SVNManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.attributeResolver"));
				oldAttributeResolver = FileManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.attributeResolver"));

				SchemaValidation.validate(oldAttributeResolver);
				LOG.debug("validated attributeResolver");


				//oldRelyingParty = SVNManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.relyingParty"));
				oldRelyingParty = FileManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.relyingParty"));
				SchemaValidation.validate(oldRelyingParty);
				LOG.debug("validated relyingParty");

				String beforeRelyingParty = new String(oldRelyingParty);
				if (newRelyingParty != null) {
					beforeRelyingParty = new String(newRelyingParty);
				}

				if (attributeName.matches("^nameid-[\\w-]+$")) {

					String nameIdURN = "";

					if (!"nameid-transient".equals(attributeName)) {
						nameIdURN = SPRegConfig.getInstance().getProperties().getProperty(attributeName);

						if (nameIdURN != null) {
							LOG.info("Relying Partying name id " + nameIdURN + " for entity " + entityId);
							newRelyingParty = TransformXML.modRelyingPartyNameId(beforeRelyingParty, entityId, nameIdURN);

							LOG.debug("validated new relyingParty");
							SchemaValidation.validate(newRelyingParty);

							LOG.debug("update local-sites.xml");

							oldMetadata = FileManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.metadata"));
//							oldMetadata = SVNManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.metadata"));
							SchemaValidation.validate(oldMetadata);
							String beforeMetadata = new String(oldMetadata);
							if (newMetadata != null) {
								beforeMetadata = new String(newMetadata);
							}

							// still needs the same URN?
							newMetadata = TransformXML.updateMetaDataRelyingParty(beforeMetadata, entityId, nameIdURN);
							SchemaValidation.validate(newMetadata);
						} else {
							LOG.info("Entity Id " + entityId + " NameId URN not found for attribute request " + attributeName);
						}
					}
				}

				String beforeAttributeRelease = new String(oldAttributeRelease);
				if (newAttributeRelease != null) {
					beforeAttributeRelease = new String(newAttributeRelease);
				}

				// Would be good to know if nameidformat is not the default
				newAttributeRelease = TransformXML.addAttributeReleaseEntity(beforeAttributeRelease, entityId, attributeName, value, isRegex);
				SchemaValidation.validate(newAttributeRelease);

				lastEntityId = new String(entityId);
				lastSpId = spId;
			}

			if (lastSpId != -1) {

				Boolean deploy = false;

				// newRelyingParty is only updated if there's a nameid change
				if (newRelyingParty != null && !oldRelyingParty.equals(newRelyingParty)) {
					LOG.debug("committing new relyingparty policy #2");
					LOG.info("Updating relying party policy for " + lastEntityId);
					FileManager.saveFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.relyingParty"), newRelyingParty);
//					SVNManager.commitFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.relyingParty"), oldRelyingParty, newRelyingParty, "Updating relying party policy for " + lastEntityId);
//					deploy = true;
				}

				if (newMetadata != null && !oldMetadata.equals(newMetadata)) {
					LOG.debug("committing new metadata #2");
					LOG.info("Updating  metatadata for " + lastEntityId);
					FileManager.saveFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.metadata"), newMetadata);
//					SVNManager.commitFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.metadata"), oldMetadata, newMetadata, "Updating  metatadata for " + lastEntityId);
//					deploy = true;  // should already be true

				}

				if (!oldAttributeRelease.equals(newAttributeRelease)) {
					LOG.debug("committing new attribute release #2");
					LOG.info("Updating attribute release policy for " + lastEntityId);
					FileManager.saveFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.attributeFilter"), newAttributeRelease);
//					SVNManager.commitFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.attributeFilter"), oldAttributeRelease, newAttributeRelease, "Updating attribute release policy for " + lastEntityId);
//					deploy = true;
				}

//				if (deploy) {
//					LOG.debug("deploy now....");
//					DeployUsingSVN.deployAll();
//					LOG.debug("deployed all in processPendingAttrChanges #2");
//				}

				psDeletePendingAttrs.setInt(1, lastSpId);
				psDeletePendingAttrs.executeUpdate();
				LOG.debug("Done working on " + lastEntityId + ".");
			}

			conn.commit();

		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (SQLException e2) {
				// ignore
			}

			if (lastErrorEmail < (new Date().getTime() - 3600000)) {
				lastErrorEmail = new Date().getTime();
				LOG.error("Unhandled exception during approved attributes update.  Sleeping for 5 minutes and trying again, but errors in the next hour will be logged as warnings to avoid e-mails.", e);
			} else {
				LOG.warn("Unhandled exception during approved attributes update.  Sleeping for 5 minutes and trying again.", e);
			}

			try {
				Thread.sleep(300000);
			} catch (InterruptedException e1) {
				// ignore
			}
		}
	}

	private void processPendingMetadataChanges() {
		LOG.debug("starting processPendingMetadataChanges()");
		String oldAttributeResolver = null;

		try {

			rsPendingSPs = psSelectPendingSPs.executeQuery();

			while (rsPendingSPs.next()) {
				String newEntityId = null;
				Boolean relyingPartyUpdate = false;

				try {
					int spId = rsPendingSPs.getInt("sp_id");
					String entityId = rsPendingSPs.getString("entity_id");
					String certificate = rsPendingSPs.getString("certificate");
					newEntityId = rsPendingSPs.getString("new_entity_id");
					int isDelete = rsPendingSPs.getInt("is_delete");
					List<AssertionConsumerService> assertionConsumerServices = new ArrayList<AssertionConsumerService>();

					if (newEntityId == null || newEntityId.trim().equals("")) {
						throw new RuntimeException("Got a null or empty entityId for spId=" + spId);
					}

					LOG.debug("Starting to work on " + newEntityId + ".");

					if (isDelete == 1) {
						String oldMetadata = FileManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.metadata"));
						String oldAttributeRelease = FileManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.attributeFilter"));
						String oldRelyingParty = FileManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.relyingParty"));

						//String oldMetadata = SVNManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.metadata"));
						//String oldAttributeRelease = SVNManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.attributeFilter"));
						//String oldRelyingParty = SVNManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.relyingParty"));

						SchemaValidation.validate(oldMetadata);
						SchemaValidation.validate(oldAttributeRelease);
						SchemaValidation.validate(oldRelyingParty);
						String newMetadata = TransformXML.removeMetadataEntity(oldMetadata, entityId);
						String newAttributeRelease = TransformXML.removeAttributeReleaseEntity(oldAttributeRelease, entityId);
						String newRelyingParty = TransformXML.removeRelyingParty(oldRelyingParty, entityId);
						SchemaValidation.validate(newMetadata);
						SchemaValidation.validate(newAttributeRelease);
						SchemaValidation.validate(newRelyingParty);

						if (!oldMetadata.equals(newMetadata)) {
							LOG.info("Deleting metadata for " + entityId);
							FileManager.saveFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.metadata"), newMetadata);
//							SVNManager.commitFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.metadata"), oldMetadata, newMetadata, "Deleting metadata for " + entityId);
						}

						if (!oldAttributeRelease.equals(newAttributeRelease)) {
							LOG.info("Deleting attribute release policy for " + entityId);
							FileManager.saveFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.attributeFilter"), newAttributeRelease);
							//SVNManager.commitFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.attributeFilter"), oldAttributeRelease, newAttributeRelease, "Deleting attribute release policy for " + entityId);
						}

						if (!oldRelyingParty.equals(newRelyingParty)) {
							LOG.info("Updating relying party policy for " + entityId);
							FileManager.saveFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.relyingParty"), newRelyingParty);
//							SVNManager.commitFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.relyingParty"), oldRelyingParty, newRelyingParty, "Updating relying party policy for " + entityId);
						}

//						DeployUsingSVN.deployAll();

						psDeleteApprovedAttrs.setInt(1, spId);
						psDeleteApprovedAttrs.executeUpdate();
						psDeletePendingAttrs.setInt(1, spId);
						psDeletePendingAttrs.executeUpdate();
						psDeleteUserSPs.setInt(1, spId);
						psDeleteUserSPs.executeUpdate();
						psDeleteContacts.setInt(1, spId);
						psDeleteContacts.executeUpdate();
						psDeletePendingAssertionConsumerServices.setInt(1, spId);
						psDeletePendingAssertionConsumerServices.executeUpdate();
						psDeletePendingSP.setInt(1, spId);
						psDeletePendingSP.executeUpdate();
						psDeleteSP.setInt(1, spId);
						psDeleteSP.executeUpdate();
						conn.commit();
					} else {

						if (certificate == null || certificate.trim().equals("")) {
							throw new RuntimeException("Got a null or empty certificate for spId=" + spId);
						}

						certificate = certificate.replaceAll("\r", "\n");
						certificate = certificate.replaceAll("\n+", "\n");
						certificate = certificate.replaceFirst("\\s*.*BEGIN.*\\s*", "");
						certificate = certificate.replaceFirst("\\s*-+END.*\\s*", "");
						certificate = certificate.trim();

						psSelectPendingAssertionConsumerServices.setInt(1, spId);
						rsPendingAssertionConsumerServices = psSelectPendingAssertionConsumerServices.executeQuery();
						while (rsPendingAssertionConsumerServices.next()) {
							String binding = rsPendingAssertionConsumerServices.getString("binding");
							String location = rsPendingAssertionConsumerServices.getString("url");
							assertionConsumerServices.add(new AssertionConsumerService(binding, location));
						}

						psSelectPendingAttrsDuringMetadataUpdate.setInt(1, spId);
						rsPendingAttrsDuringMetadataUpdate = psSelectPendingAttrsDuringMetadataUpdate.executeQuery();
						Map<String, Attribute> attrs = new HashMap<String, Attribute>();
						String nameIdURN = SPRegConfig.getInstance().getProperties().getProperty("nameid-transient");
						while (rsPendingAttrsDuringMetadataUpdate.next()) {
							String attributeName = rsPendingAttrsDuringMetadataUpdate.getString("name");
							String value = rsPendingAttrsDuringMetadataUpdate.getString("value");
							boolean isRegex = rsPendingAttrsDuringMetadataUpdate.getBoolean("is_value_regex");

							LOG.debug("entity=" + entityId + " attribute name=" + attributeName);
							if (!attrs.containsKey(attributeName)) {
								attrs.put(attributeName, new Attribute(attributeName));
							}

							// check attr name for nameid format to see if metadata should be transient or unspecified
							if (attributeName != null && attributeName.matches("^nameid-[\\w-]+$")) {

								if (!"nameid-transient".equals(attributeName)) {
									LOG.debug("looking for nameid");
									String nameIdProp = SPRegConfig.getInstance().getProperties().getProperty(attributeName);
									LOG.debug("nameIdProp=" + nameIdProp);
									if (nameIdProp != null) {
										LOG.debug("name id being set");
										nameIdURN = nameIdProp;
										relyingPartyUpdate = true;
									}
									LOG.debug("nameIdURN=" + nameIdURN);
								}
							}

							if (value != null && !value.equals("")) {
								attrs.get(attributeName).addValue(new AttributeValue(value, isRegex));
							}
						}

						if (assertionConsumerServices.size() == 0) {
							throw new RuntimeException("No ACS URLs for " + newEntityId);
						}

						String oldMetadata = FileManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.metadata"));
						String oldAttributeRelease = FileManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.attributeFilter"));
						String oldRelyingParty = FileManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.relyingParty"));
//						String oldMetadata = SVNManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.metadata"));
//						String oldAttributeRelease =SVNManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.attributeFilter"));
//						String oldRelyingParty = SVNManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.relyingParty"));

						SchemaValidation.validate(oldMetadata);
						SchemaValidation.validate(oldAttributeRelease);
						SchemaValidation.validate(oldRelyingParty);

						LOG.debug("enityid=" + entityId);
						LOG.debug("newEntityId=" + newEntityId);
						LOG.debug("certificate=" + certificate);
						LOG.debug("assertionConsumerServices=" + assertionConsumerServices.size());
						LOG.debug("nameIdURN=" + nameIdURN);

						String newMetadata = TransformXML.addOrUpdateMetadataEntity(oldMetadata, newEntityId, entityId, certificate, assertionConsumerServices, nameIdURN);

						String newAttributeRelease = TransformXML.replaceAttributeReleaseEntity(oldAttributeRelease, newEntityId, entityId, attrs);

						String newRelyingParty = oldRelyingParty;

						// if the nameIdURN is always unspecified if not trans can use this - otherwise will have to use previous code
						// or set the nameid earlier when going through the attributes
						if (relyingPartyUpdate) {
							if (nameIdURN != null) {
								LOG.debug("updating relyingparty");
								newRelyingParty = TransformXML.modRelyingPartyNameId(newRelyingParty, entityId, nameIdURN);

							}
						}

						LOG.debug("validating newMetadata");
						SchemaValidation.validate(newMetadata);

						LOG.debug("validating newAttributeRelease");
						SchemaValidation.validate(newAttributeRelease);

						LOG.debug("validating newRelyingParty");
						SchemaValidation.validate(newRelyingParty);

						oldAttributeResolver = FileManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.attributeResolver"));
//						oldAttributeResolver = SVNManager.getFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.attributeResolver"));
						SchemaValidation.validate(oldAttributeResolver);
						LOG.debug("validated attributeResolver");

						if (!oldMetadata.equals(newMetadata)) {
							LOG.info("Updating metadata for " + entityId);
							FileManager.saveFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.metadata"), newMetadata);
//							SVNManager.commitFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.metadata"), oldMetadata, newMetadata, "Updating metadata for " + entityId);
						}

						if (!oldAttributeRelease.equals(newAttributeRelease)) {
							LOG.info("Updating attribute release policy for " + entityId);
							FileManager.saveFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.attributeFilter"), newAttributeRelease);
//							SVNManager.commitFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.attributeFilter"), oldAttributeRelease, newAttributeRelease, "Updating attribute release policy for " + entityId);
						}

						if (!oldRelyingParty.equals(newRelyingParty)) {
							LOG.info("Updating relying party policy for " + entityId);
							FileManager.saveFile(SPRegConfig.getInstance().getProperties().getProperty("file.location.relyingParty"), newRelyingParty);
//							SVNManager.commitFile(SPRegConfig.getInstance().getProperties().getProperty("svn.location.relyingParty"), oldRelyingParty, newRelyingParty, "Updating relying party policy for " + entityId);
						}

//						LOG.debug("Deploying all in processingMetaDataChanges");
//						DeployUsingSVN.deployAll();

						if (newEntityId != null && !newEntityId.equals(entityId)) {
							psUpdateSP.setString(1, newEntityId);
							psUpdateSP.setInt(2, spId);
							psUpdateSP.executeUpdate();
						}

						psDeletePendingAttrsApproved.setInt(1, spId);
						psDeletePendingAttrsApproved.executeUpdate();
						psDeletePendingAssertionConsumerServices.setInt(1, spId);
						psDeletePendingAssertionConsumerServices.executeUpdate();
						psDeletePendingSP.setInt(1, spId);
						psDeletePendingSP.executeUpdate();
						conn.commit();
					}

					LOG.info("Done working on " + newEntityId + ".");
					newEntityId = null;
				} catch (Exception e) {
					try {
						conn.rollback();
					} catch (SQLException e2) {
						// ignore
					}

					if (newEntityId == null) {
						// must have been a db access issue.  throw exception
						throw e;
					}

					if (lastErrorEmail < (new Date().getTime() - 3600000)) {
						lastErrorEmail = new Date().getTime();
						LOG.error("Error updating metadata/attributes for " + newEntityId + ".  This will be retried, but first sleeping for 5 minutes and then continuing processing, but errors in the next hour will be logged as warnings to avoid e-mails.", e);
					} else {
						LOG.warn("Error updating metadata/attributes for " + newEntityId + ".", e);
					}

					try {
						Thread.sleep(300000);
					} catch (InterruptedException e1) {
						// ignore
					}
				}
			}

			conn.commit();

		} catch (Exception e) {

			if (lastErrorEmail < (new Date().getTime() - 3600000)) {
				lastErrorEmail = new Date().getTime();
				LOG.error("Unhandled exception during metadata/attributes update.  Sleeping for 5 minutes and trying again, but errors in the next hour will be logged as warnings to avoid e-mails.", e);
			} else {
				LOG.warn("Unhandled exception during metadata/attributes update.  Sleeping for 5 minutes and trying again.", e);
			}

			try {
				Thread.sleep(300000);
			} catch (InterruptedException e1) {
				// ignore
			}
		}
	}

	protected void shutdown() {
		LOG.info("SHUTDOWN: Closing connections.");

		if (conn != null) {
			try {
				conn.rollback();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (rsPendingAssertionConsumerServices != null) {
			try {
				rsPendingAssertionConsumerServices.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (rsPendingSPs != null) {
			try {
				rsPendingSPs.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (rsPendingAttrsDuringMetadataUpdate != null) {
			try {
				rsPendingAttrsDuringMetadataUpdate.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (rsPendingAttrsNotDuringMetadataUpdate != null) {
			try {
				rsPendingAttrsNotDuringMetadataUpdate.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (psSelectPendingSPs != null) {
			try {
				psSelectPendingSPs.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (psSelectPendingAssertionConsumerServices != null) {
			try {
				psSelectPendingAssertionConsumerServices.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (psDeletePendingSP != null) {
			try {
				psDeletePendingSP.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (psDeletePendingAssertionConsumerServices != null) {
			try {
				psDeletePendingAssertionConsumerServices.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (psUpdateSP != null) {
			try {
				psUpdateSP.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (psDeleteSP != null) {
			try {
				psDeleteSP.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (psDeleteContacts != null) {
			try {
				psDeleteContacts.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (psDeleteUserSPs != null) {
			try {
				psDeleteUserSPs.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (psDeletePendingAttrs != null) {
			try {
				psDeletePendingAttrs.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (psDeleteApprovedAttrs != null) {
			try {
				psDeleteApprovedAttrs.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (psSelectPendingAttrsDuringMetadataUpdate != null) {
			try {
				psSelectPendingAttrsDuringMetadataUpdate.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (psSelectPendingAttrsNotDuringMetadataUpdate != null) {
			try {
				psSelectPendingAttrsDuringMetadataUpdate.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (psDeletePendingAttrsApproved != null) {
			try {
				psDeletePendingAttrsApproved.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				// ignore
			}
		}

		LOG.info("SHUTDOWN: Finished closing connections.");
		this.isShutDown = true;
	}

	private void init() throws IOException, ClassNotFoundException, SQLException {
		this.isShutDown = false;
		Properties props = SPRegConfig.getInstance().getProperties();

		Class.forName(props.getProperty("db.driver"));
		conn = DriverManager.getConnection(props.getProperty("db.url"), props.getProperty("db.username"),
				props.getProperty("db.password"));
		conn.setAutoCommit(false);

		psSelectPendingSPs = conn.prepareStatement("select s.id as sp_id, s.entity_id, p.id as pending_sp_id, p.certificate, p.new_entity_id, p.is_delete from sps s, pending_sps p where s.id=p.sp_id and (s.is_draft <> '1' or s.is_draft is null) order by p.id");
		psSelectPendingAssertionConsumerServices = conn.prepareStatement("select binding, url from pending_sp_assertions where sp_id=? order by is_default desc");
		psDeletePendingSP = conn.prepareStatement("delete from pending_sps where sp_id=?");
		psDeletePendingAssertionConsumerServices = conn.prepareStatement("delete from pending_sp_assertions where sp_id=?");
		psUpdateSP = conn.prepareStatement("update sps set entity_id=? where id=?");
		psDeleteSP = conn.prepareStatement("delete from sps where id=?");
		psDeleteContacts = conn.prepareStatement("delete from sp_contacts where sp_id=?");
		psDeleteUserSPs = conn.prepareStatement("delete from sps_users where sp_id=?");
		psDeletePendingAttrs = conn.prepareStatement("delete from pending_sp_attrs where sp_id=?");
		psDeletePendingAttrsApproved = conn.prepareStatement("delete from pending_sp_attrs where is_approved='1' and sp_id=?");
		psDeleteApprovedAttrs = conn.prepareStatement("delete from approved_sp_attrs where sp_id=?");
		psSelectPendingAttrsDuringMetadataUpdate = conn.prepareStatement("select a.attributeID as name, p.value, p.is_value_regex from attributes a, pending_sp_attrs p where a.id=p.attribute_id and p.is_approved = '1' and sp_id=?");
		psSelectPendingAttrsNotDuringMetadataUpdate = conn.prepareStatement("select s.id as sp_id, s.entity_id, a.attributeID as name, p.value, p.is_value_regex from attributes a, pending_sp_attrs p, sps s where s.id = p.sp_id and a.id=p.attribute_id and p.is_approved = '1' and s.id not in (select sp_id from pending_sps) order by s.id");

	}
}
