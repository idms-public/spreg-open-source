package edu.duke.oit.idms.shibboleth.spreg;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

/**
 * @author shilen
 */
public class SchemaValidation {
  
  /**
   * @param xml
   * @throws IOException 
   * @throws SAXException 
   */
  public static void validate(String xml) throws IOException, SAXException {
    
    String[] schemas = { 
        "xml.xsd", 
        "xmldsig-core-schema.xsd", 
        "xenc-schema.xsd", 
        "shibboleth-2.0-resource.xsd",
        "shibboleth-2.0-services.xsd",
        "shibboleth-2.0-afp.xsd",
        "shibboleth-2.0-afp-mf-basic.xsd",
        "shibboleth-afp-mf-saml.xsd",
        "saml-schema-assertion-2.0.xsd",
        "saml-schema-metadata-2.0.xsd",
        "shibboleth-attribute-encoder.xsd",
        "shibboleth-attribute-resolver-ad.xsd",
        "shibboleth-attribute-resolver-dc.xsd",
        "shibboleth-attribute-resolver-pc.xsd",
        "shibboleth-attribute-resolver.xsd",
        "spring-beans.xsd",
        "spring-context.xsd",
        "spring-util.xsd"
        };
   
    SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

    Source[] sources = new Source[schemas.length];
    
    for (int i = 0; i < schemas.length; i++) {
      URL url = SchemaValidation.class.getClassLoader().getResource(schemas[i]);
      Source schemaFile = new StreamSource(url.openStream());      
      sources[i] = schemaFile;
    }
    
    Schema schema = factory.newSchema(sources);

    Validator validator = schema.newValidator();
    validator.validate(new StreamSource(new StringReader(xml)));
  }
}
