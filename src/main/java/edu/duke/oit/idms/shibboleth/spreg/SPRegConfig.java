package edu.duke.oit.idms.shibboleth.spreg;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

/**
 * @author shilen
 */
public class SPRegConfig {
  
  private static SPRegConfig config = null;
  private Properties props = null;

  private SPRegConfig() throws IOException {
    props = new Properties();
    URL url = SPRegConfig.class.getClassLoader().getResource("properties.conf");
    props.load(url.openStream());
  }
  
  /**
   * @return config manager
   * @throws IOException 
   */
  public static SPRegConfig getInstance() throws IOException {
    if (config == null) {
      config = new SPRegConfig();
    }
    
    return config;
  }
  
  /**
   * @return properties
   */
  public Properties getProperties() {
    return props;
  }
}
