package edu.duke.oit.idms.shibboleth.spreg;

import java.util.LinkedList;
import java.util.List;

/**
 * @author shilen
 */
public class Attribute {

  private List<AttributeValue> values;
  private String name;
  
  /**
   * @param name
   */
  public Attribute(String name) {
    values = new LinkedList<AttributeValue>();
    this.name = name;
  }
  
  /**
   * @param value
   */
  public void addValue(AttributeValue value) {
    values.add(value);
  }
  
  /**
   * @return name of attribute
   */
  public String getName() {
    return name;
  }
  
  /**
   * @return values
   */
  public List<AttributeValue> getValues() {
    return values;
  }
}
