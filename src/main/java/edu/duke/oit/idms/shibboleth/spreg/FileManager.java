package edu.duke.oit.idms.shibboleth.spreg;

import java.io.*;
import java.nio.file.*;

public class FileManager {

    public static String getFile(String filePath) throws IOException {
        // open things

        InputStream input = new FileInputStream(filePath);
        BufferedReader buffer = new BufferedReader(new InputStreamReader(input));
        String line = buffer.readLine();
        StringBuilder builder = new StringBuilder();

        while(line != null) {
            builder.append(line).append("\n");
            line = buffer.readLine();
        }

        String xml = builder.toString();

        return xml;
    }


    public static void saveFile(String filePath, String xml) throws IOException {

        Files.write(Paths.get(filePath), xml.getBytes(), StandardOpenOption.CREATE);

    }
}
