package edu.duke.oit.idms.shibboleth.spreg;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;

/**
 * @author shilen
 */
public class DeployUsingSVN {
  
  private static Logger LOG = Logger.getLogger(DeployUsingSVN.class);
  
  private static Map<String, Boolean> returnStatuses;
  private static Map<String, Exception> errors;
  
  /**
   *
   */
  public static class DeploymentThread implements Runnable {

    private String hostname;
    
    /**
     * @param hostname
     */
    public DeploymentThread(String hostname) {
      this.hostname = hostname;
    }
    
    public void run() {
      
      Connection connection = null;
      Session session = null;
      
      try {
        returnStatuses.put(hostname, false);
        String username = SPRegConfig.getInstance().getProperties().getProperty("deploy.username");
        String password = SPRegConfig.getInstance().getProperties().getProperty("deploy.password");
        String command = SPRegConfig.getInstance().getProperties().getProperty("deploy.command");
        
        connection = new Connection(hostname);
        connection.connect();

        // todo change this to auth with key or no password and use the key setup
        if (!connection.authenticateWithPassword(username, password)) {
          Exception e = new RuntimeException("Authentication failed to " + hostname + " during deployment.");
          errors.put(hostname, e);
          return;
        }
        
        session = connection.openSession();  
        session.execCommand(command);

        // Read the results
        StringBuilder sb = new StringBuilder();
        InputStream stdout = new StreamGobbler(session.getStdout());
        BufferedReader br = new BufferedReader(new InputStreamReader(stdout));
        String line = br.readLine();
        while( line != null ) {
            sb.append( line + "\n" );
            line = br.readLine(); 
        }
        
        // looks like we're getting null return codes sometimes.  it's possible that we may miss failures..
        if (session.getExitStatus() != null  && !session.getExitStatus().equals(0)) {
          Exception e = new RuntimeException("Exit status for deployment to " + hostname + " is " + session.getExitStatus() + ".  " +
              "Return string is:\n" + sb.toString());
          errors.put(hostname, e);
          return;
        }
        
        if (sb.toString().contains("Skipped")) {
          // looks like there may be an issue?
          Exception e = new RuntimeException("Exit status for deployment to " + hostname + " is 0 but an update may have been skipped.  "  +
              "Return string is:\n" + sb.toString());
          errors.put(hostname, e);
          return;
        }
        
        returnStatuses.put(hostname, true);
        LOG.info("Done deployment on " + hostname + ".");
        
      } catch (Exception e) {
        errors.put(hostname, e);
      } finally {
        
        if (session != null) {
          try {
            session.close();
          } catch (Exception e) {
            // ignore
          }
        }
        
        if (connection != null) {
          try {
            connection.close();
          } catch (Exception e) {
            // ignore
          }
        }
      }
    }
  
  }

  /**
   * @throws Exception 
   */
  public static void deployAll() throws Exception {
    returnStatuses = new HashMap<String, Boolean>();
    errors = new HashMap<String, Exception>();
    List<String> pending = new ArrayList<String>();
    
    int hostCount = 1;
    while (true) {
      String hostname = SPRegConfig.getInstance().getProperties().getProperty("deploy.host." + hostCount);
      if (hostname == null) {
        break;
      }
      
      new Thread(new DeploymentThread(hostname)).start();
      pending.add(hostname);
      hostCount++;
    }
    
    // wait for all threads to complete or error
    while (pending.size() > 0) {
      // sleep for a while
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        // ignore
      }
      
      Iterator<String> iter = new ArrayList<String>(pending).iterator();
      while (iter.hasNext()) {
        String hostname = iter.next();
        if (returnStatuses.get(hostname) == true || errors.containsKey(hostname)) {
          pending.remove(hostname);
        }
      }
    }
    
    // now if there are errors, let's log them all as warns and throw the first one
    Exception e = null;
    Iterator<String> iter = errors.keySet().iterator();
    while (iter.hasNext()) {
      String hostname = iter.next();
      LOG.warn("Deploy error on " + hostname + ".", errors.get(hostname));
      
      if (e == null) {
        e = errors.get(hostname);
      }
    }
    
    if (e != null) {
      throw e;
    }
  }
}
