package edu.duke.oit.idms.shibboleth.spreg;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.*;




/**
 * @author shilen
 */
public class TransformXML {

  public static final String RELYING_PARTY_IDS = "c:relyingPartyIds";

  /**
   * @param metadataXML
   * @param entityId
   * @param oldEntityId
   * @param certificate
   * @param assertionConsumerServices
   * @return modified xml
   * @throws XPathExpressionException
   * @throws SAXException
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws TransformerFactoryConfigurationError
   * @throws TransformerException
   */
  public static String addOrUpdateMetadataEntity(String metadataXML, String entityId, String oldEntityId, String certificate,
                                                 List<AssertionConsumerService> assertionConsumerServices, String nameIdURN)
          throws XPathExpressionException, SAXException, IOException, ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException {

    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    Document doc = builder.parse(new ByteArrayInputStream(metadataXML.getBytes()));

    Node oldEntity = null;

    if (oldEntityId != null && !oldEntityId.equals("") && !oldEntityId.equals(entityId)) {
      oldEntity = getMetadataEntity(doc, oldEntityId);
    }

    Node newOrUpdateEntity = getMetadataEntity(doc, entityId);

    if (oldEntity != null && newOrUpdateEntity != null) {
      throw new RuntimeException("Looks like we're renaming " + oldEntityId + " to " + entityId +
              " but both entityIds exist in the metadata file.");
    }

    if (oldEntity == null && newOrUpdateEntity == null) {
      // this must be an add
      return addMetadataEntity(doc, entityId, certificate, assertionConsumerServices, nameIdURN);
    }

    // otherwise an update
    if (oldEntity != null && newOrUpdateEntity == null) {
      // let's rename and update
      oldEntity.getAttributes().getNamedItem("entityID").setNodeValue(entityId);
      return updateMetadataEntity(doc, oldEntity, certificate, assertionConsumerServices, nameIdURN);
    } else {
      // let's update
      return updateMetadataEntity(doc, newOrUpdateEntity, certificate, assertionConsumerServices, nameIdURN);
    }
  }

  /**
   * @param doc
   * @param entity
   * @param certificate
   * @param assertionConsumerServices
   * @return modified xml
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException
   * @throws XPathExpressionException
   * @throws TransformerFactoryConfigurationError
   * @throws TransformerException
   */
  public static String updateMetadataEntity(Document doc, Node entity, String certificate,
                                            List<AssertionConsumerService> assertionConsumerServices, String nameIdURN)
          throws ParserConfigurationException, SAXException, IOException, XPathExpressionException, TransformerFactoryConfigurationError, TransformerException {

    XPath xpath = XPathFactory.newInstance().newXPath();

    // see if we're allowing KeyName values.  only allowed if currently has one...
    boolean allowKeyName = false;
    NodeList keyNameNodes = (NodeList) xpath.evaluate("SPSSODescriptor/KeyDescriptor/KeyInfo/KeyName", entity, XPathConstants.NODESET);
    if (keyNameNodes.getLength() > 0) {
      allowKeyName = true;
    }

    Node ssoDescriptorNode = (Node) xpath.evaluate("SPSSODescriptor", entity, XPathConstants.NODE);

    // first remove old elements
    NodeList keyDescriptorNodes = (NodeList) xpath.evaluate("SPSSODescriptor/KeyDescriptor", entity, XPathConstants.NODESET);
    for (int i = 0; i < keyDescriptorNodes.getLength(); i++) {
      Node keyDescriptorNode = keyDescriptorNodes.item(i);
      ssoDescriptorNode.removeChild(keyDescriptorNode);
    }

    NodeList assertionConsumerServicesNodes = (NodeList) xpath.evaluate("SPSSODescriptor/AssertionConsumerService", entity, XPathConstants.NODESET);
    for (int i = 0; i < assertionConsumerServicesNodes.getLength(); i++) {
      Node assertionConsumerServicesNode = assertionConsumerServicesNodes.item(i);
      ssoDescriptorNode.removeChild(assertionConsumerServicesNode);
    }

    Node insertBeforeNode = null;
    NodeList nameIdFormatNodes = (NodeList) xpath.evaluate("SPSSODescriptor/NameIDFormat", entity, XPathConstants.NODESET);
    for (int i = 0; i < nameIdFormatNodes.getLength(); i++) {
      Node nameIdFormatNode = nameIdFormatNodes.item(i);
      ssoDescriptorNode.removeChild(nameIdFormatNode);
    }

    // now update
    addMetadataCertificate(doc, ssoDescriptorNode, insertBeforeNode, certificate, allowKeyName);
    addNameId(doc, ssoDescriptorNode, nameIdURN);
    addMetadataAssertionConsumerServices(doc, ssoDescriptorNode, assertionConsumerServices);

    return transformXML(doc);
  }

  /**
   * @param metadataXML
   * @param entityId
   * @return modified xml
   * @throws SAXException
   * @throws IOException
   * @throws XPathExpressionException
   * @throws ParserConfigurationException
   * @throws TransformerException
   */
  public static String removeMetadataEntity(String metadataXML, String entityId)
          throws SAXException, IOException, XPathExpressionException, ParserConfigurationException, TransformerException {

    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    Document doc = builder.parse(new ByteArrayInputStream(metadataXML.getBytes()));
    Node entityToModify = getMetadataEntity(doc, entityId);

    if (entityToModify == null) {
      return metadataXML;
    }

    // remove node
    entityToModify.getParentNode().removeChild(entityToModify);

    return transformXML(doc);
  }

  /**
   * @param doc
   * @param entityId
   * @param certificate
   * @param assertionConsumerServices
   * @return modified xml
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException
   * @throws XPathExpressionException
   * @throws TransformerFactoryConfigurationError
   * @throws TransformerException
   */
  public static String addMetadataEntity(Document doc, String entityId, String certificate,
                                         List<AssertionConsumerService> assertionConsumerServices, String nameIdURN)
          throws ParserConfigurationException, SAXException, IOException, XPathExpressionException, TransformerFactoryConfigurationError, TransformerException {

    Node root = getMetadataRoot(doc);

    // add node
    Element entity = doc.createElement("EntityDescriptor");
    entity.setAttribute("entityID", entityId);

    Element ssoDescriptor = doc.createElement("SPSSODescriptor");
    ssoDescriptor.setAttribute("AuthnRequestsSigned", "0");
    ssoDescriptor.setAttribute("protocolSupportEnumeration", "urn:oasis:names:tc:SAML:2.0:protocol urn:oasis:names:tc:SAML:1.1:protocol urn:oasis:names:tc:SAML:1.0:protocol http://schemas.xmlsoap.org/ws/2003/07/secext");

    addMetadataCertificate(doc, ssoDescriptor, null, certificate, false);

    Element nameIdFormat1 = doc.createElement("NameIDFormat");
    nameIdFormat1.setTextContent("urn:mace:shibboleth:1.0:nameIdentifier");
    Element nameIdFormat2 = doc.createElement("NameIDFormat");
    nameIdFormat2.setTextContent(nameIdURN);
    ssoDescriptor.appendChild(nameIdFormat1);
    ssoDescriptor.appendChild(nameIdFormat2);

    addMetadataAssertionConsumerServices(doc, ssoDescriptor, assertionConsumerServices);

    entity.appendChild(ssoDescriptor);
    root.appendChild(entity);

    return transformXML(doc);
  }

  private static Node getMetadataEntity(Document doc, String entityId) throws XPathExpressionException {
    XPath xpath = XPathFactory.newInstance().newXPath();
    Node entity = (Node) xpath.evaluate("/EntitiesDescriptor/EntityDescriptor[@entityID='" + entityId + "']", doc, XPathConstants.NODE);
    return entity;
  }

  private static Node getMetadataRoot(Document doc) throws XPathExpressionException {
    XPath xpath = XPathFactory.newInstance().newXPath();
    Node root = (Node) xpath.evaluate("/EntitiesDescriptor", doc, XPathConstants.NODE);
    return root;
  }

  private static Node getAttributeReleaseRoot(Document doc) throws XPathExpressionException {
    XPath xpath = XPathFactory.newInstance().newXPath();
    Node root = (Node) xpath.evaluate("/AttributeFilterPolicyGroup", doc, XPathConstants.NODE);
    return root;
  }

  private static Node getRelyingPartyListRoot(Document doc) throws XPathExpressionException {
    XPath xpath = XPathFactory.newInstance().newXPath();
    Node root = (Node) xpath.evaluate("/beans/list", doc, XPathConstants.NODE);

    return root;
  }

  private static String transformXML(Document doc) throws TransformerFactoryConfigurationError, TransformerException {
    TransformerFactory factory = TransformerFactory.newInstance();
    factory.setAttribute("indent-number", new Integer(2));
    Transformer transformer = factory.newTransformer();
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

    Writer writer = new StringWriter();
    StreamResult result = new StreamResult(writer);
    DOMSource source = new DOMSource(doc);
    transformer.transform(source, result);
    String retVal = writer.toString();
    retVal = retVal.replaceAll("\n\\s*\n", "\n");

    return retVal;
  }



  private static void addMetadataCertificate(Document doc, Node ssoDescriptor, Node insertBeforeNode, String certificate, boolean allowKeyName) {

    Element keyDescriptor = doc.createElement("KeyDescriptor");
    Element keyInfo = doc.createElement("ds:KeyInfo");

    if (allowKeyName && !certificate.contains("\n")) {
      Element keyName = doc.createElement("ds:KeyName");
      keyName.setTextContent(certificate);
      keyInfo.appendChild(keyName);
    } else {

      if (!certificate.contains("\n")) {
        throw new RuntimeException("This does not appear to be a valid certificate: " + certificate);
      }

      Element x509Data = doc.createElement("ds:X509Data");
      Element x509Certificate = doc.createElement("ds:X509Certificate");
      x509Certificate.setTextContent("\n" + certificate + "\n            ");
      x509Data.appendChild(x509Certificate);
      keyInfo.appendChild(x509Data);
    }

    keyDescriptor.appendChild(keyInfo);

    if (insertBeforeNode == null) {
      ssoDescriptor.appendChild(keyDescriptor);
    } else {
      ssoDescriptor.insertBefore(keyDescriptor, insertBeforeNode);
    }
  }

  private static void addMetadataAssertionConsumerServices(Document doc, Node ssoDescriptor,
                                                           List<AssertionConsumerService> assertionConsumerServices) {

    Iterator<AssertionConsumerService> iter = assertionConsumerServices.iterator();
    int count = 1;

    while (iter.hasNext()) {
      AssertionConsumerService acs = iter.next();

      Element e = doc.createElement("AssertionConsumerService");
      e.setAttribute("index", "" + count);

      if (count == 1) {
        e.setAttribute("isDefault", "true");
      }

      e.setAttribute("Binding", acs.getBinding());
      e.setAttribute("Location", acs.getLocation());
      ssoDescriptor.appendChild(e);
      count++;
    }


  }

  private static void addNameId(Document doc, Node ssoDescriptor, String nameIdURN) {
    Element nameIdIdent = doc.createElement("NameIDFormat");
    nameIdIdent.setTextContent("urn:mace:shibboleth:1.0:nameIdentifier");
    ssoDescriptor.appendChild(nameIdIdent);
    Element nameIdFormat = doc.createElement("NameIDFormat");
    nameIdFormat.setTextContent(nameIdURN);
    ssoDescriptor.appendChild(nameIdFormat);
  }

  /**
   * @param doc
   * @param entityId
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException
   * @throws TransformerFactoryConfigurationError
   * @throws TransformerException
   * @throws XPathExpressionException
   */
  public static void removeAttributeReleaseEntity(Document doc, String entityId)
          throws ParserConfigurationException, SAXException, IOException, TransformerFactoryConfigurationError, TransformerException, XPathExpressionException {

    XPath xpath = XPathFactory.newInstance().newXPath();

    // first remove where the policy is only defined for that one entity
    NodeList rules = (NodeList) xpath.evaluate("/AttributeFilterPolicyGroup/AttributeFilterPolicy/PolicyRequirementRule[@value='" + entityId + "']", doc, XPathConstants.NODESET);
    for (int i = 0; i < rules.getLength(); i++) {
      Node rule = rules.item(i);
      Node policy = rule.getParentNode();
      policy.getParentNode().removeChild(policy);
    }

    // next remove if there are multiple entities for the policy
    rules = (NodeList) xpath.evaluate("/AttributeFilterPolicyGroup/AttributeFilterPolicy/PolicyRequirementRule[@type='basic:OR']/Rule[@value='" + entityId + "']", doc, XPathConstants.NODESET);
    for (int i = 0; i < rules.getLength(); i++) {
      Node rule = rules.item(i);
      Element fullRule = (Element) rule.getParentNode();
      Element policy = (Element) fullRule.getParentNode();
      fullRule.removeChild(rule);

      // now if this policy requirement rule only has one child rule, let's modify it to remove the OR condition.
      NodeList childRules = fullRule.getElementsByTagName("basic:Rule");
      if (childRules.getLength() == 1) {
        Element onlyRule = (Element) childRules.item(0);
        NamedNodeMap attrs = onlyRule.getAttributes();

        Element newFullRule = doc.createElement("PolicyRequirementRule");
        policy.replaceChild(newFullRule, fullRule);

        for (int j = 0; j < attrs.getLength(); j++) {
          Node attr = attrs.item(j);
          String name = attr.getNodeName();
          String value = attr.getNodeValue();
          newFullRule.setAttribute(name, value);
        }
      }
    }
  }

  /**
   * @param attributeReleaseXML
   * @param entityId
   * @return modified xml
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException
   * @throws TransformerFactoryConfigurationError
   * @throws TransformerException
   * @throws XPathExpressionException
   */
  public static String removeAttributeReleaseEntity(String attributeReleaseXML, String entityId)
          throws ParserConfigurationException, SAXException, IOException, TransformerFactoryConfigurationError, TransformerException, XPathExpressionException {

    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    Document doc = builder.parse(new ByteArrayInputStream(attributeReleaseXML.getBytes()));

    removeAttributeReleaseEntity(doc, entityId);
    return transformXML(doc);
  }

  /**
   * @param attributeReleaseXML
   * @param entityId
   * @param oldEntityId
   * @param attrs
   * @return modified xml
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException
   * @throws XPathExpressionException
   * @throws TransformerFactoryConfigurationError
   * @throws TransformerException
   */
  public static String replaceAttributeReleaseEntity(String attributeReleaseXML, String entityId, String oldEntityId, Map<String, Attribute> attrs)
          throws ParserConfigurationException, SAXException, IOException, XPathExpressionException, TransformerFactoryConfigurationError, TransformerException {

    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    Document doc = builder.parse(new ByteArrayInputStream(attributeReleaseXML.getBytes()));

    // first remove old release policies for this entity
    removeAttributeReleaseEntity(doc, entityId);

    // if it's a rename, remove according to the old name
    if (oldEntityId != null && !oldEntityId.equals(entityId)) {
      removeAttributeReleaseEntity(doc, oldEntityId);
    }

    attributeReleaseXML = transformXML(doc);

    //Node root = getAttributeReleaseRoot(doc);

    Iterator<Attribute> attrsIter = attrs.values().iterator();
    while (attrsIter.hasNext()) {
      Attribute attr = attrsIter.next();

      if (attr.getValues().size() == 0) {
        attributeReleaseXML = addAttributeReleaseEntity(attributeReleaseXML, entityId, attr.getName(), null, false);
      } else {
        Iterator<AttributeValue> attrValuesIter = attr.getValues().iterator();
        while (attrValuesIter.hasNext()) {
          AttributeValue value = attrValuesIter.next();
          attributeReleaseXML = addAttributeReleaseEntity(attributeReleaseXML, entityId, attr.getName(), value.getValue(), value.isRegex());
        }
      }
    }

    return attributeReleaseXML;
    
    /*
    // now add the new one based on the passed in attributes
    Element policy = doc.createElement("AttributeFilterPolicy");
    root.appendChild(policy);
    policy.setAttribute("id", entityId);
    
    Element rule = doc.createElement("PolicyRequirementRule");
    policy.appendChild(rule);
    rule.setAttribute("xsi:type", "basic:AttributeRequesterString");
    rule.setAttribute("value", entityId);
    
    Iterator<Attribute> attrsIter = attrs.values().iterator();
    while (attrsIter.hasNext()) {
      Attribute attr = attrsIter.next();
      String name = attr.getName();
      Element attrElement = doc.createElement("AttributeRule");
      policy.appendChild(attrElement);
      attrElement.setAttribute("attributeID", name);

      List<AttributeValue> attrValues = attr.getValues();
      if (attrValues.size() == 0) {
        Element permitValueElement = doc.createElement("PermitValueRule");
        attrElement.appendChild(permitValueElement);
        permitValueElement.setAttribute("xsi:type", "basic:ANY");
      } else if (attrValues.size() == 1) {
        AttributeValue value = attrValues.get(0);
        Element permitValueElement = doc.createElement("PermitValueRule");
        attrElement.appendChild(permitValueElement);
        if (value.isRegex()) {
          permitValueElement.setAttribute("regex", value.getValue());
          permitValueElement.setAttribute("xsi:type", "basic:AttributeValueRegex");
        } else {
          permitValueElement.setAttribute("value", value.getValue());
          permitValueElement.setAttribute("xsi:type", "basic:AttributeValueString");
        }
      } else {
        Element permitValueElement = doc.createElement("PermitValueRule");
        attrElement.appendChild(permitValueElement);
        permitValueElement.setAttribute("xsi:type", "basic:OR");
        Iterator<AttributeValue> attrValuesIter = attrValues.iterator();
        while (attrValuesIter.hasNext()) {
          AttributeValue value = attrValuesIter.next();
          Element ruleElement = doc.createElement("basic:Rule");
          permitValueElement.appendChild(ruleElement);
          if (value.isRegex()) {
            ruleElement.setAttribute("regex", value.getValue());
            ruleElement.setAttribute("xsi:type", "basic:AttributeValueRegex");
          } else {
            ruleElement.setAttribute("value", value.getValue());
            ruleElement.setAttribute("xsi:type", "basic:AttributeValueString");
          }
        }
      }
    }

    return transformXML(doc);*/
  }

  /**
   * @param attributeReleaseXML
   * @param entityId
   * @param attributeName
   * @param value
   * @param isRegex
   * @return modified xml
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IOException
   * @throws TransformerException
   * @throws TransformerFactoryConfigurationError
   * @throws XPathExpressionException
   */
  public static String addAttributeReleaseEntity(String attributeReleaseXML, String entityId, String attributeName, String value, boolean isRegex)
          throws ParserConfigurationException, SAXException, IOException, TransformerFactoryConfigurationError, TransformerException, XPathExpressionException {

    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    Document doc = builder.parse(new ByteArrayInputStream(attributeReleaseXML.getBytes()));
    XPath xpath = XPathFactory.newInstance().newXPath();

    Node root = getAttributeReleaseRoot(doc);

    // see if a policy with the entityId as the policy id exists.
    Element policy = (Element) xpath.evaluate("/AttributeFilterPolicyGroup/AttributeFilterPolicy[@id='" + entityId + "']", doc, XPathConstants.NODE);
    if (policy == null) {
      policy = doc.createElement("AttributeFilterPolicy");
      root.appendChild(policy);
      policy.setAttribute("id", entityId);

      Element rule = doc.createElement("PolicyRequirementRule");
      policy.appendChild(rule);
      rule.setAttribute("xsi:type", "basic:AttributeRequesterString");
      rule.setAttribute("value", entityId);
    }

    // see if the attribute is already in the policy
    Element attributeRule = (Element) xpath.evaluate("AttributeRule[@attributeID='" + attributeName + "']", policy, XPathConstants.NODE);
    if (attributeRule == null) {
      attributeRule = doc.createElement("AttributeRule");
      policy.appendChild(attributeRule);
      attributeRule.setAttribute("attributeID", attributeName);
      Element permitValueRule = doc.createElement("PermitValueRule");
      attributeRule.appendChild(permitValueRule);
      if (value == null || value.equals("")) {
        permitValueRule.setAttribute("xsi:type", "basic:ANY");
      } else {
        if (isRegex) {
          permitValueRule.setAttribute("regex", value);
          permitValueRule.setAttribute("xsi:type", "basic:AttributeValueRegex");
        } else {
          permitValueRule.setAttribute("value", value);
          permitValueRule.setAttribute("xsi:type", "basic:AttributeValueString");
        }
      }

      return transformXML(doc);
    }

    // check if there are no changes to be made or if all values are requested then it's a simple change
    {
      Element permitValueRule = (Element) xpath.evaluate("PermitValueRule", attributeRule, XPathConstants.NODE);
      if (permitValueRule == null) {
        throw new RuntimeException("Attribute release policy for " + entityId + " contains an attribute rule without a permit value rule.");
      }

      if (permitValueRule.getAttribute("xsi:type").equals("basic:ANY")) {
        // already releasing so just return
        return attributeReleaseXML;
      }

      if (value == null || value.equals("")) {
        attributeRule.removeChild(permitValueRule);
        Element newPermitValueRule = doc.createElement("PermitValueRule");
        attributeRule.appendChild(newPermitValueRule);
        newPermitValueRule.setAttribute("xsi:type", "basic:ANY");
        return transformXML(doc);
      }

      if (permitValueRule.getAttribute("xsi:type").equals("basic:AttributeValueString") && !isRegex &&
              permitValueRule.getAttribute("value").equals(value)) {
        // again, we're already good it seems
        return attributeReleaseXML;
      }

      if (permitValueRule.getAttribute("xsi:type").equals("basic:AttributeValueRegex") && isRegex &&
              permitValueRule.getAttribute("regex").equals(value)) {
        // again, we're already good it seems
        return attributeReleaseXML;
      }
    }

    // check if there are no changes to be made if the permit rule is an OR.
    {
      NodeList permitValueRules = (NodeList) xpath.evaluate("PermitValueRule[@type='basic:OR']/Rule", attributeRule, XPathConstants.NODESET);
      for (int i = 0; i < permitValueRules.getLength(); i++) {
        Element permitValueRule = (Element) permitValueRules.item(i);
        if (permitValueRule.getAttribute("xsi:type").equals("basic:AttributeValueString") && !isRegex &&
                permitValueRule.getAttribute("value").equals(value)) {
          // again, we're already good it seems
          return attributeReleaseXML;
        }

        if (permitValueRule.getAttribute("xsi:type").equals("basic:AttributeValueRegex") && isRegex &&
                permitValueRule.getAttribute("regex").equals(value)) {
          // again, we're already good it seems
          return attributeReleaseXML;
        }
      }
    }

    // seems like we need to add the attribute value...
    {
      Element permitValueRule = (Element) xpath.evaluate("PermitValueRule", attributeRule, XPathConstants.NODE);
      if (permitValueRule.getAttribute("xsi:type").equals("basic:OR")) {
        Element newPermitValueRule = doc.createElement("basic:Rule");
        permitValueRule.appendChild(newPermitValueRule);
        if (isRegex) {
          newPermitValueRule.setAttribute("regex", value);
          newPermitValueRule.setAttribute("xsi:type", "basic:AttributeValueRegex");
        } else {
          newPermitValueRule.setAttribute("value", value);
          newPermitValueRule.setAttribute("xsi:type", "basic:AttributeValueString");
        }
      } else {
        NamedNodeMap attrs = permitValueRule.getAttributes();

        Element replacePermitValueRule = doc.createElement("PermitValueRule");
        attributeRule.replaceChild(replacePermitValueRule, permitValueRule);
        replacePermitValueRule.setAttribute("xsi:type", "basic:OR");
        Element existingPermitValueRule = doc.createElement("basic:Rule");
        replacePermitValueRule.appendChild(existingPermitValueRule);

        for (int j = 0; j < attrs.getLength(); j++) {
          Node attr = attrs.item(j);
          String name = attr.getNodeName();
          String val = attr.getNodeValue();
          existingPermitValueRule.setAttribute(name, val);
        }

        Element newPermitValueRule = doc.createElement("basic:Rule");
        replacePermitValueRule.appendChild(newPermitValueRule);
        if (isRegex) {
          newPermitValueRule.setAttribute("regex", value);
          newPermitValueRule.setAttribute("xsi:type", "basic:AttributeValueRegex");
        } else {
          newPermitValueRule.setAttribute("value", value);
          newPermitValueRule.setAttribute("xsi:type", "basic:AttributeValueString");
        }
      }
    }

    return transformXML(doc);
  }

  /***
   * searches RelyingParty for a entityId and returns the relyingPartyIds string. e.g. #{{'id_a','id_b'}}
   * @param doc
   * @param entityId
   * @return
   * @throws SAXException
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws TransformerException
   * @throws XPathExpressionException
   */
  public static String getRelyingPartyIds(Document doc, String entityId)
          throws SAXException, IOException, ParserConfigurationException, TransformerException, XPathExpressionException {
    String relyingPartyIds = null;

    // would like to use util:list/bean or list/bean
    NodeList nl = doc.getElementsByTagName("bean");

    if (nl == null) {
      return null;
    }

    for (int j = 0; j < nl.getLength(); j++) {
      Node n = nl.item(j);

      NamedNodeMap nm = n.getAttributes();

      Attr b = (Attr) nm.getNamedItem("parent");
      if (b != null) {
        if (!"RelyingPartyByName".equals(b.getValue())) {
          continue;
        }
      } else {
        continue;
      }

      Attr c = (Attr) nm.getNamedItem("c:relyingPartyIds");
      if (c != null) {
        relyingPartyIds = c.getValue();

        String id_string = relyingPartyIds.replaceAll("[#{} ']", "");
        String[] ids = id_string.split(",");

        for (String id : ids) {
          if (id.equals(entityId)) {
            return relyingPartyIds;
          }
        }
      }
    }

    return null;
  }

  /***
   * returns the relyingParty node
   * @param doc
   * @param relyingPartyIds
   * @return
   * @throws SAXException
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws TransformerException
   * @throws XPathExpressionException
   */
  public static Node getRelyingPartyEntity(Document doc, String relyingPartyIds)
          throws SAXException, IOException, ParserConfigurationException, TransformerException, XPathExpressionException {

    XPath xpath = XPathFactory.newInstance().newXPath();

    // this worked!!!
    Node entity = (Node) xpath.evaluate("/beans/list/bean[@relyingPartyIds=\"" + relyingPartyIds + "\"]", doc, XPathConstants.NODE);

    return entity;

  }

  public static int relyingPartyIdsLength(String relyingPartyIds) {
    int length = 0;

    if (relyingPartyIds == null) {
      return 0;
    }

    String id_string = relyingPartyIds.replaceAll("[#{} ']", "");
    String[] ids = id_string.split(",");

    if (ids == null) {
      return 0;
    } else {
      // might be 0
      return ids.length;
    }
  }

  public static String removeRelyingParty(String relyingPartyXML, String entityId)
          throws SAXException, IOException, ParserConfigurationException, TransformerException, XPathExpressionException {

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document doc = builder.parse(new ByteArrayInputStream(relyingPartyXML.getBytes()));

    String relyingPartyIds = getRelyingPartyIds(doc, entityId);

    // nothing to do
    if (relyingPartyIds == null) {
      return transformXML(doc);
    }

    // too complicated
    if (relyingPartyIdsLength(relyingPartyIds) > 1) {
      return transformXML(doc);
    }

    Node entity = getRelyingPartyEntity(doc, relyingPartyIds);

    if (entity == null) {
      return transformXML(doc);
    }

    entity.getParentNode().removeChild(entity);

    return transformXML(doc);
  }

  public static String addRelyingPartyEntity(Document doc, String entityId, String nameid)
          throws SAXException, IOException, ParserConfigurationException, TransformerException, XPathExpressionException {

    if (entityId == null || nameid == null) {
      return transformXML(doc);
    }

    // how to do this
    // first create an element
    // the root should be
    Node list = getRelyingPartyListRoot(doc);

    Element bean = doc.createElement("bean");
    list.appendChild(bean);
    bean.setAttribute("parent", "RelyingPartyByName");
    String formattedEntityId = "#{{'" + entityId + "'}}";
    bean.setAttribute("c:relyingPartyIds", formattedEntityId);

    Element prop = doc.createElement("property");
    bean.appendChild(prop);
    prop.setAttribute("name", "profileConfigurations");

    Element prop_list = doc.createElement("list");
    prop.appendChild(prop_list);

    Element prop_bean = doc.createElement("bean");
    String formattedNameId = "#{{'" + nameid + "'}}";
    prop_list.appendChild(prop_bean);
    prop_bean.setAttribute("parent", "SAML2.SSO");
    prop_bean.setAttribute("p:encryptAssertions", "true");  // this might be the default so doesn't need setting?
    prop_bean.setAttribute("p:nameIDFormatPrecedence", formattedNameId);

    // create element <property name="profileConfigurations">
    // create element bean
    // attribute parent="SAML2.SSO"
    // attribute p:encryptAssertions="true"
    // attribute p:nameIDFormatPrecedence="#{{'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified'}}"

    return transformXML(doc);
  }


  public static String updateNameId(Document doc, String relyingPartyIds, String nameId)
          throws SAXException, IOException, ParserConfigurationException, TransformerException, XPathExpressionException {

    XPath xpath = XPathFactory.newInstance().newXPath();

    Node entity = (Node) xpath.evaluate("/beans/list/bean[@relyingPartyIds=\"" + relyingPartyIds + "\"]/property[@name='profileConfigurations']/list/bean", doc, XPathConstants.NODE);

    // too tricky would be more than one in the precedence

    Element elem = (Element) entity;
    String formatedNameId = "#{{'" + nameId + "'}}";
    elem.setAttribute("p:nameIDFormatPrecedence", formatedNameId);

    return transformXML(doc);
  }

  public static String modRelyingPartyNameId(String relyingPartyXML, String entityId, String nameId)
          throws SAXException, IOException, ParserConfigurationException, TransformerException, XPathExpressionException {

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document doc = builder.parse(new ByteArrayInputStream(relyingPartyXML.getBytes()));

    String relyingPartyIds = getRelyingPartyIds(doc, entityId);

    // Add the nameid info
    if (relyingPartyIds == null) {
      String newParty = addRelyingPartyEntity(doc, entityId, nameId);
      return newParty;
    }

    if (relyingPartyIdsLength(relyingPartyIds) > 1) {
      return relyingPartyXML;

    }

    if (relyingPartyIdsLength(relyingPartyIds) == 1) {
      String newParty = updateNameId(doc, relyingPartyIds, nameId);
      return newParty;
    }

    return transformXML(doc);
  }

  public static String updateMetaDataRelyingParty(String metadata, String entityId, String nameId)
      throws SAXException, IOException, ParserConfigurationException, TransformerException, XPathExpressionException {

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document doc = builder.parse(new ByteArrayInputStream(metadata.getBytes()));

    XPath xpath = XPathFactory.newInstance().newXPath();
    Node entity = (Node) xpath.evaluate("/EntitiesDescriptor/EntityDescriptor[@entityID='" + entityId + "']/SPSSODescriptor", doc, XPathConstants.NODE);

    if (entity != null) {
      NodeList nl = entity.getChildNodes();

      for (int i = 0; i < nl.getLength(); i++) {
        Node cn = nl.item(i);
        if (cn.getNodeName().equals("NameIDFormat")) {


          if (cn.getTextContent() == null) {
            // NameIDFormat text null - skipping
            continue;
          }

          if (cn.getTextContent().equals("urn:mace:shibboleth:1.0:nameIdentifier")) {
            // NameIDFormat nameIdentifier not the NameIdFormat node we're looking for
          } else if (cn.getTextContent().equals(nameId)) {
            // No nameid metadata entity update needed for entity
            break;
          } else if (cn.getTextContent().equals("urn:oasis:names:tc:SAML:2.0:nameid-format:transient")) {

            // maybe do a replace?

            // Updating metadata entity nameid format for entity
            Element nameIdFormat = doc.createElement("NameIDFormat");

            Node tn = doc.createTextNode(nameId);
            nameIdFormat.appendChild(tn);

            cn.getParentNode().replaceChild(nameIdFormat, cn);

            break;
          } else {
            // No nameid metadatda entity update because not transient so not sure what to do
            break;
          }
        }
      }
    }

    return transformXML(doc);
  }
}
