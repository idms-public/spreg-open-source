package edu.duke.oit.idms.shibboleth.spreg;

/**
 * @author shilen
 */
public class AssertionConsumerService {

  private String binding;
  private String location;
  
  /**
   * @param binding
   * @param location
   */
  public AssertionConsumerService(String binding, String location) {
    this.binding = binding;
    this.location = location;
  }
  
  /**
   * @return binding
   */
  public String getBinding() {
    return binding;
  }
  
  /**
   * @return location
   */
  public String getLocation() {
    return location;
  }
}
