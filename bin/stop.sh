#!/bin/sh

echo "Stopping..."

get_pid() {
  PID=`ps -eo pid,cmd | grep edu.duke.oit.idms.shibboleth.spreg.SPReg | grep -v grep | awk '{print $1}'`
}

get_pid

if [ $PID ]
then
  kill $PID
fi

while [ $PID ]
do
  echo "Waiting to shutdown...";
  sleep 1
  get_pid
done
