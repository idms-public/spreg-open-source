#!/bin/sh

get_pid() {
  PID=`ps -eo pid,cmd | grep edu.duke.oit.idms.shibboleth.spreg.SPReg | grep -v grep | awk '{print $1}'`
}

export APP_HOME=/srv/idms/applications/spreg/build/install/spreg

for i in `ls $APP_HOME/lib/*.jar $APP_HOME/schema $APP_HOME/conf`; do CLASSPATH=$CLASSPATH:$i; done

get_pid

if [ $PID ]
then
  echo "Already running..."
  exit
fi


sleep 11

nohup java -classpath $CLASSPATH -Xms256m -Xmx256m edu.duke.oit.idms.shibboleth.spreg.SPReg &
sleep 2
tail -f /srv/idms/logs/applications/spreg/output.log
